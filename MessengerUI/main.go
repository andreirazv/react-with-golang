package main

import (
	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/websocket"
)

func main()  {
	serveFrontend()
	serveBackend()
	err := http.ListenAndServe("localhost:1324",nil)
	if err !=nil{
		log.Fatalf("could not create the webserver: %v",err)
	}
}
func serveFrontend(){
fs := http.FileServer(http.Dir("./frontend/build/"))

http.Handle("/",fs)
}

type Client struct{
	Conn * websocket.Conn
}

func serveBackend(){
	var clients []Client
	http.HandleFunc("/ws",func(w http.ResponseWriter, r *http.Request){

		upgrader:=websocket.Upgrader{}


		client :=Client{}
		conn,err := upgrader.Upgrade(w,r,nil)
		if err !=nil{
			log.Fatalf("could not upgrade connection: %v",err)
		}
		client.Conn=conn
		clients=append(clients,client)

		for {
			messageType,msg,_:=client.Conn.ReadMessage()
				for _,cl:=range clients {
					err = cl.Conn.WriteMessage(messageType, []byte(msg))

					if err != nil {
						log.Fatalf("could not send message: %v", err)
					}

					fmt.Println("message recived: %s (%d message type)", string(msg), int(messageType))
				}
		}

	})

}
